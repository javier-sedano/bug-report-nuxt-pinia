// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt',
  ],
  piniaPersistedstate: {
    storage: 'localStorage'
  },
  watch: [
    'node_modules/javier-sedano-ufe5-store-library-pinia/src/stores/**/*.ts',
  ],
  runtimeConfig: {
    public: {
      baseUrl: `http://localhost:${process.env.PORT}/`,
    },
  },
});
